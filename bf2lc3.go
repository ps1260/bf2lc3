// bf2lc3
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Please give a BF filepath!")
		return
	}
	bfcode := load_file_strarr(os.Args[1])
	var bfinstarr []byte
	for i, _ := range bfcode {
		for j, _ := range bfcode[i] {
			if is_inst(bfcode[i][j]) {
				bfinstarr = append(bfinstarr, bfcode[i][j])
			}
		}
	}
	var lc3code []string
	opened := 0
	var bracketstack []int
	lc3code = append(lc3code, ".ORIG x3000")
	//Load R1 to hold the data pointer
	//R0 holds the value within M[data pointer]
	lc3code = append(lc3code, "AND R1, R1, #0")
	lc3code = append(lc3code, "LD R1, #1")
	lc3code = append(lc3code, "BRnzp #1")
	lc3code = append(lc3code, ".FILL x5000")
	for i, _ := range bfinstarr {
		switch bfinstarr[i] {
		case '>':
			lc3code = append(lc3code, "ADD R1, R1, #1")
		case '<':
			lc3code = append(lc3code, "ADD R1, R1, #-1")
		case '+':
			lc3code = append(lc3code, "LDR R0, R1, #0")
			lc3code = append(lc3code, "ADD R0, R0, #1")
			lc3code = append(lc3code, "STR R0, R1, #0")
		case '-':
			lc3code = append(lc3code, "LDR R0, R1, #0")
			lc3code = append(lc3code, "ADD R0, R0, #-1")
			lc3code = append(lc3code, "STR R0, R1, #0")
		case '.':
			lc3code = append(lc3code, "LDR R0, R1, #0")
			lc3code = append(lc3code, "OUT")
		case ',':
			lc3code = append(lc3code, "GETC")
			lc3code = append(lc3code, "STR R0, R1, #0")
		case '[':
			opened++
			bracketstack = append(bracketstack, opened)
			lc3code = append(lc3code, "LDR R0, R1, #0")
			lc3code = append(lc3code, "BRz CLABEL"+strconv.Itoa(opened))
			lc3code = append(lc3code, "OLABEL"+strconv.Itoa(opened))
		case ']':
			lc3code = append(lc3code, "LDR R0, R1, #0")
			lc3code = append(lc3code, "BRnp OLABEL"+strconv.Itoa(bracketstack[len(bracketstack)-1]))
			lc3code = append(lc3code, "CLABEL"+strconv.Itoa(bracketstack[len(bracketstack)-1]))
			bracketstack = bracketstack[:len(bracketstack)-1]
		default:
			panic("ERROR: Non-instruction in process")
		}
	}
	lc3code = append(lc3code, "HALT")
	lc3code = append(lc3code, ".END")
	write_file_strarr("out.asm", lc3code)
}

func is_inst(in byte) bool {
	return in == '>' || in == '<' || in == '+' || in == '-' ||
		in == '.' || in == ',' || in == '[' || in == ']'
}

func load_file_strarr(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("Bad filename give")
	}
	defer file.Close()
	var sourcecode []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		sourcecode = append(sourcecode, scanner.Text())
	}
	return sourcecode
}

func write_file_strarr(filename string, data []string) {
	file, err := os.Create(filename)
	panic_on_error(err)
	defer file.Close()
	writer := bufio.NewWriter(file)
	for i := range data {
		fmt.Fprintln(writer, data[i])
	}
	writer.Flush()
	return
}

func panic_on_error(in error) {
	if in != nil {
		panic(in)
	}
}
